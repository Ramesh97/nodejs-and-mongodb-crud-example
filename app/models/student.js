const mongoose = require('mongoose');

const StudentSchema = mongoose.Schema({
    studentId : String,
    studentName : String,
    studentAddress : String,
    studentNIC : String
},{
    timestamps : true
});

module.exports = mongoose.model('Student',StudentSchema);