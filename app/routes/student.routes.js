module.exports = (app) => {
    const student = require('../controller/student.controller.js');

    // Create a new Student
    app.post('/student', student.create);

    // Retrieve all Students
    app.get('/student', student.findAll);

    // Retrieve a single Student with StudentId
    app.get('/student/:studentId', student.findOne);

    // Update a Student with StudentId
    app.put('/student/:studentId', student.update);

    // Delete a Student with StudentId
    app.delete('/student/:studentId', student.delete);

}