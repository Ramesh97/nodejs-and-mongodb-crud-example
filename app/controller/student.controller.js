const Student = require('../models/student');

// Create and Save a new Student
exports.create = (req, res) => {

    // Validate request
    if(!req.body.studentName) {
        return res.status(400).send({
            message: "Student Name content can not be empty"
        });
    }

    // Create a Student
    const student = new Student({
        studentId: req.body.studentId || "Untitled Student", 
        studentName: req.body.studentName,
        studentAddress: req.body.studentAddress,
        studentNIC : req.body.studentNIC
    });

    // Save Student in the database
    student.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Student."
        });
    });

};

// Retrieve and return all Students from the database.
exports.findAll = (req, res) => {

    Student.find()
    .then(student => {
        res.send(student);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving students."
        });
    });

};

// Find a single Student with a StudentId
exports.findOne = (req, res) => {

    Student.findById(req.params.studentId)
    .then(student => {
        if(!student){
            return res.status(404).send({
                message: "Student not found with StudentId " + req.params.noteId
            });  
        }
        res.send(student);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Student not found with StudentId " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Student with StudentId " + req.params.noteId
        });
    });
};

// Update a Student identified by the StudentId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.studentName) {
        return res.status(400).send({
            message: "Student Name can not be empty"
        });
    }

    // Find note and update it with the request body
    Student.findByIdAndUpdate(req.params.studentId, {
        studentId: req.body.studentId || "Untitled Student", 
        studentName: req.body.studentName,
        studentAddress: req.body.studentAddress,
        studentNIC : req.body.studentNIC
    }, {new: true})
    .then(student => {
        if(!student) {
            return res.status(404).send({
                message: "Student not found with id " + req.params.noteId
            });
        }
        res.send(student);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Student not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Error updating Student with id " + req.params.noteId
        });
    });
};

// Delete a Student with the specified StudentId in the request
exports.delete = (req, res) => {
    Student.findByIdAndRemove(req.params.studentId)
    .then(student => {
        if(!student) {
            return res.status(404).send({
                message: "Student not found with id " + req.params.noteId
            });
        }
        res.send({message: "Student deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Student not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Could not delete Student with id " + req.params.noteId
        });
    });
};